# Order API System. ( http://localhost:8080 )
Application performs order creation, distance calculation,order fetch and patching the order status.

## Software requirement for the Order API system.

- [Docker](https://www.docker.com/) as the container service to isolate the environment.
- [Php](https://php.net/) to develop backend support.
- [Laravel](https://laravel.com) as the server framework/controller
- [MySQL](https://mysql.com/) as the database layer
- [NGINX](https://docs.nginx.com/nginx/admin-guide/content-cache/content-caching/) as a proxy/content-caching layer

## How to Install & Run with docker
1. Clone the repo.
2. For Building Docker Containers, migrations and test cases run file ./start.sh. This will create required containers and migrate tables in created DB and run test cases.
3. We have used the Google Distance Matrix API for distance calculation
4. Set Google Distance Matrix Api key `GOOGLE_API_KEY` in environment(.env) file located in root.
   We need to get API key from the url `https://cloud.google.com/maps-platform/routes/` after login and then create new project and get the API for the same.

## Manually Starting the docker and test Cases

1. You can run docker-compose -d up from terminal
2. Server is accessible at http://localhost:8080
3. Run manual test case suite by `docker exec order_api php ./vendor/phpunit/phpunit/phpunit ./tests/` and it will run unit tests and integration tests.



## Codebase Structure (./)

**./app**
- Contains Controller , Models, Validators, helpers, Services for the Order API's.

**.env**
- This is used for project configuring settings like Database, Google API, etc.
    Set GOOGLE_API_KEY in this file to perform distance calculation while creating the order.   

**./tests**
- This folder contains Unit and Integration test cases.

## Swagger
Open URL `http://localhost:8080/api/documentation` for API's documentions.

## Assumptions
- Docker is already installed on the system.