<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderAddRequest;
use App\Http\Requests\OrderListRequest;
use App\Http\Requests\OrderPatchRequest;
use App\Http\Resources\OrderResource;
use App\Http\Response\ResponseHelper;
use App\Http\Services\OrderServices;
use Illuminate\Http\JsonResponse;
use OpenApi\Annotations as OA;

class OrderController extends Controller
{
    protected $responseHelper;
    protected $OrderServices;

    public function __construct(OrderServices $OrderServices, ResponseHelper $responseHelper)
    {
        $this->OrderServices = $OrderServices;
        $this->responseHelper = $responseHelper;
    }

    /**
     * @OA\Info(
     *     version="1.0",
     *     title="Order Management"
     * )
     */

    /**
     * @OA\Get(
     *     path="/orders?page=1&limit=4",
     *     summary="Fetch all orders against given page and limit",
     *     @OA\Response(
     *         response=400,
     *         description="INVALID_PARAMETERS",
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="ORDER_FETCHED_SUCCESSFULLY",
     *          @OA\JsonContent(ref="#/components/schemas/create_order")
     *     )
     *
     * )
     */

    /**
     *          @OA\Schema(
     *              schema="create_order",
     *                  type = "array",
     *                  @OA\Items(
     *                  @OA\Property(
     *                     property="id",
     *                     type="integer"
     *                  ),
     *                  @OA\Property(
     *                      property="distance",
     *                      type="integer"
     *                  ),
     *                  @OA\Property(
     *                      property="status",
     *                      type="string"
     *                  ))
     *          )
     */
    public function index(OrderListRequest $request)
    {
        try {
            $page = (int) $request->get('page', 1);
            $limit = (int) $request->get('limit', 1);

            $orders = $this->OrderServices->fetchAllOrders($page, $limit);
            return $this->responseHelper->successResponse('SUCCESS', JsonResponse::HTTP_OK, $orders);
        } catch (\Exception $e) {
            return $this->responseHelper->errorResponse($e->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Post(
     *     path="/orders",
     *     summary="Creates a new order",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="origin",
     *                     type = "array",
     *                     items = "string"
     *                 ),
     *                 @OA\Property(
     *                     property="destination",
     *                     type = "array",
     *                     items = "string"
     *                 ),
     *                 example={"origin": {"26.897667","74.900089"}, "destination": {"28.345434","74.900089"}}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *          response=200,
     *          description="OK",
     *          @OA\JsonContent(ref="#/components/schemas/create_order")
     *     ),
     *      @OA\Response(
     *          response=400,
     *          description="MUST_HAVE_TWO_ORIGIN/DESTINATION_CORDINATES",
     *     ),
     *
     * )
     */

    public function store(OrderAddRequest $request)
    {
        try {
            $order = $this->OrderServices->addOrder($request);
            if ($order instanceof \App\Order) {
                return $this->responseHelper->formatOrderResponse($order);
            } else {
                $messages = $this->OrderServices->error;
                $errorCode = $this->OrderServices->errorCode;
                return $this->responseHelper->errorResponse($messages, $errorCode);
            }
        } catch (\Exception $e) {
            return $this->responseHelper->errorResponse($e->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\Patch(
     *     path="/orders/6",
     *     summary="Patch order status against given ID in url",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/json",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="status",
     *                     type = "string",
     *                 ),
     *                 example={"status": "TAKEN"}
     *             )
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Order status updated from UNASSIGNED to TAKEN",
     *     ),
     *     @OA\Response(
     *         response=409,
     *         description="ORDER_HAS_ALREADY_BEEN_TAKEN"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="ORDER_DOES_NOT_EXIST"
     *     )
     *
     * )
     */
    public function updateStatus(OrderPatchRequest $request, $id)
    {
        try {
            $order = $this->OrderServices->getOrder($id);
            if ($order === false) {
                return $this->responseHelper->errorResponse('ORDER_NOT_FOUND', JsonResponse::HTTP_NOT_FOUND);
            }
            if ($this->OrderServices->takeOrder($id) === false) {
                return $this->responseHelper->errorResponse('ORDER_ALREADY_TAKEN', JsonResponse::HTTP_CONFLICT);
            }
            return $this->responseHelper->successResponse('SUCCESS', JsonResponse::HTTP_OK);
        } catch (\Exception $e) {
            return $this->responseHelper->errorResponse($e->getMessage(), JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
