<?php

namespace App\Http\Requests;

class OrderAddRequest extends AbstractFormRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'origin' => [
                'required','array',
                function ($attribute, $value, $fail) {
                    if (count($value) !== 2 || empty($value[0]) || empty($value[1]) || !is_numeric($value[0]) || !is_numeric($value[1])) {
                        $fail('MUST_HAVE_TWO_ORIGIN_CORDINATES');
                    }
                },
            ],
            'destination' => [
                'required','array',
                function ($attribute, $value, $fail) {
                    if (count($value) !== 2 || empty($value[0]) || empty($value[1]) || !is_numeric($value[0]) || !is_numeric($value[1])) {
                        $fail('MUST_HAVE_TWO_DESTINATION_CORDINATES');
                    }
                },
            ],
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'origin.required' => 'ORIGIN_LAT_LONG_REQUIRED',
            'destination.required' => 'DESTINATION_LAT_LONG_REQUIRED',
            'origin.array' => 'ORIGIN_SHOULD_BE_ARRAY',
            'destination.array' => 'DESTINATION_SHOULD_BE_ARRAY'
        ];
    }
}
