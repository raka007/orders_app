<?php

namespace App\Http\Response;

use Illuminate\Http\JsonResponse;

class ResponseHelper
{
    protected $messageHelper;

    public function __construct(\App\Helpers\MessageHelper $messageHelper)
    {
        $this->messageHelper = $messageHelper;
    }

    public function errorResponse($message, $responseCode = JsonResponse::HTTP_BAD_REQUEST, $translateMessage = true)
    {
        if (true === $translateMessage) {
            $message = $this->messageHelper->getMessage($message) ?: $message;
        }
        $response = ['error' => $message];
        return response()->json($response, $responseCode);
    }

    public function successResponse($message, $responseCode = JsonResponse::HTTP_OK, $res = [], $translateMessage = true)
    {
        if (true === $translateMessage) {
            $message = $this->messageHelper->getMessage($message) ?: $message;
        }
        if(empty($res)){
            $response = ['status' => $message];
        }else{
            $response = $res;
        }
        
        
        return response()->json($response, $responseCode);
    }

    public function formatOrderResponse(\App\Order $order)
    {
        return [
            'id' => $order->id,
            'distance' => $order->distance,
            'status' => $order->status
        ];
    }
}
