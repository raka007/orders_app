<?php

namespace App\Http\Services;

use App\Helpers\DistanceHelper;
use App\Validators\CoordinatesValidator;
use Illuminate\Http\JsonResponse;

class OrderServices
{
    /**
     *
     * @var distanceHelper
     */
    protected $distanceHelper;
    public $error = null;
    public $errorCode;
    protected $coordinatesValidator;
    protected $orderRepo;

    public function __construct(
        CoordinatesValidator $coordinatesValidator,
        \App\Http\Repository\orderRepo $orderRepo,
        DistanceHelper $distanceHelper
    ) {
        $this->coordinatesValidator = $coordinatesValidator;
        $this->orderRepo = $orderRepo;
        $this->distanceHelper = $distanceHelper;
    }

    public function addOrder($reqData)
    {
        $startLat = $reqData->origin[0];
        $startLongi = $reqData->origin[1];
        $endLat = $reqData->destination[0];
        $endLongi = $reqData->destination[1];

        $validateCoordinates = $this->coordinatesValidator
            ->validate($startLat, $startLongi, $endLat, $endLongi);

        if (!$validateCoordinates) {
            $this->error = $this->coordinatesValidator->getError();
            $this->errorCode = JsonResponse::HTTP_BAD_REQUEST;
            return false;
        }

        $origin = $startLat . "," . $startLongi;
        $destination = $endLat . "," . $endLongi;
        $distance = $this->distanceHelper->calculateDistance($origin, $destination);

        if (!is_int($distance) || $distance <= 0) {
            $this->error = $distance;
            $this->errorCode = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;
            return false;
        }

        $rec = [
            'origin_start_latitude' => $startLat,
            'origin_end_longitude' => $startLongi,
            'destination_start_latitude' => $endLat,
            'destination_end_longitude' => $endLongi,
            'distance' => $distance
        ];
        $order = $this->orderRepo->create($rec);
        return $order;
    }

    public function fetchAllOrders($page, $limit)
    {
        $page = (int) $page;
        $limit = (int) $limit;
        $orders = [];

        if ($limit > 0 && $page > 0) {
            $skip = ($page - 1) * $limit;
            $orders = $this->orderRepo->getAllOrders($skip, $limit);
        }
        return $orders;
    }

    public function getOrder($id)
    {
        return $this->orderRepo->getOrderById($id);
    }

    public function takeOrder($orderId)
    {
        return $this->orderRepo->takeOrder($orderId);
    }
}
