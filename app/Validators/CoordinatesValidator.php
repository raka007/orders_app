<?php

namespace App\Validators;

class CoordinatesValidator
{
    const LATITUDE_LOWER_RANGE_LIMIT = -90;
    const LATITUDE_UPPER_RANGE_LIMIT = 90;
    const LONGITUDE_LOWER_RANGE_LIMIT = -180;
    const LONGITUDE_UPPER_RANGE_LIMIT = 180;
    protected $error;

    public function getError()
    {
        return $this->error;
    }

    
    public function validate($startLat, $startLongi, $endLat, $endLongi)
    {
        if ($startLat == $endLat && $startLongi == $endLongi) {
            $this->error = 'SAME_REQUESTED_ORIGIN_DESTINATION';
        } elseif (!$startLat || !$startLongi || !$endLat || !$endLongi) {
            $this->error = 'REQUEST_PARAMETER_MISSING';
        } elseif ($startLat < self::LATITUDE_LOWER_RANGE_LIMIT
            || $startLat > self::LATITUDE_UPPER_RANGE_LIMIT
            || $endLat < self::LATITUDE_LOWER_RANGE_LIMIT
            || $endLat > self::LATITUDE_UPPER_RANGE_LIMIT
        ) {
            $this->error = 'LATITUDE_OUT_OF_RANGE';
        } elseif ($startLongi < self::LONGITUDE_LOWER_RANGE_LIMIT
            || $startLongi > self::LONGITUDE_UPPER_RANGE_LIMIT
            || $endLongi < self::LONGITUDE_LOWER_RANGE_LIMIT
            || $endLongi > self::LONGITUDE_UPPER_RANGE_LIMIT
        ) {
            $this->error = 'LONGITUDE_OUT_OF_RANGE';
        } elseif (!is_numeric($startLat)
            || !is_numeric($endLat)
            || !is_numeric($startLongi)
            || !is_numeric($endLongi)
        ) {
            $this->error = 'INVALID_PARAMETERS';
        }
        return $this->error ? false : true;
    }
}
