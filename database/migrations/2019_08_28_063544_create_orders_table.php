<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('origin_start_latitude');
            $table->string('origin_end_longitude');
            $table->string('destination_start_latitude');
            $table->string('destination_end_longitude');
            $table->bigInteger('distance');
            $table->enum('status', ['UNASSIGNED', 'TAKEN'])->default('UNASSIGNED');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
