<?php

use App\Order;
use Illuminate\Database\Seeder;

class OrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i = 0; $i < 5; $i++) {
            $start_lat = $faker->latitude();
            $end_lat = $faker->latitude();
            $start_long = $faker->longitude();
            $end_long = $faker->longitude();
            $distance = $this->distance($start_lat, $start_long, $end_lat, $end_long);
            $queryState = false;
            $queryState = DB::table('orders')->insert([
                'origin_start_latitude' => $start_lat,
                'origin_end_longitude' => $start_long,
                'destination_start_latitude' => $end_lat,
                'destination_end_longitude' => $end_long,
                'distance' => $distance,
                'status' => $i % 2 == 0 ? Order::UNASSIGNED_ORDER_STATUS : Order::ASSIGNED_ORDER_STATUS,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ]);
            if ($queryState) {
                echo "record inserted successfully...";
            } else {
                echo "record insertion failed...";
            }
        }
    }

    public function distance($lat1, $lon1, $lat2, $lon2)
    {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $distanceInMetre = $dist * 60 * 1.1515 * 1.609344 * 1000;

        return $distanceInMetre;
    }
}
