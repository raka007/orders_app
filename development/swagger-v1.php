<?php
/**
 * @SWG\Swagger(
 *     schemes={"http"},
 *     host=API_HOST,
 *     basePath="/",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Laravel and Swagger",
 *         description="Laravel and Swagger",
 *         termsOfService="",
 *         @SWG\Contact(
 *             email="rakesh.kumar01@nagarro.com"
 *         ),
 *     ),
 * )
 */
