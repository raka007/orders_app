#!/usr/bin/env bash
sudo apt-get update
sudo apt install -y curl

echo "Configuring Pre requisites... "
docker-compose down && docker-compose up -d 

echo "Configuring Dependencies... "
docker-compose run composer install --ignore-platform-reqs --quiet
docker exec order_api php artisan optimize:clear

echo "Starting Migrations & Data Seeding... "
sudo chmod 777 -R /var/www/
docker exec order_api php artisan migrate
docker exec order_api php artisan db:seed  --class=OrderTableSeeder

echo "\n Starting Unit test cases..."
docker exec order_api php ./vendor/phpunit/phpunit/phpunit ./tests/Unit

echo "\n Starting Intergration test cases..."
docker exec order_api php ./vendor/phpunit/phpunit/phpunit ./tests/Feature

exit 0
