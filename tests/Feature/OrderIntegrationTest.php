<?php

namespace App\Test\Feature\ApiController;

use App\Order;
use Illuminate\Http\JsonResponse;
use Tests\TestCase;

class OrderIntegrationTest extends TestCase
{
    protected static $allowedOrderStatus = [
        Order::UNASSIGNED_ORDER_STATUS,
        Order::ASSIGNED_ORDER_STATUS,
    ];

    public function tearDown(): void
    {
        \Mockery::close();
    }

    public function testOrderCreationPositiveCase()
    {
        echo "\n  <---- Running Order Test Cases ----> \n";
        echo "\n  <---- Create Order Positive Test Case for Valid Input ----> \n";
        
        $validInput = [
            'origin' => ['26.905727', '75.745567'],
            'destination' => ['26.906900', '75.747912'],
        ];

        $response = $this->json('POST', '/orders', $validInput);
        $data = (array) $response->getData();
        
        echo "\n\t <---- should get Status: 200 ----> \n";
        $response->assertStatus(JsonResponse::HTTP_OK);
    }

    public function testOrderCreationInvalidParameters()
    {
        echo "\n  <---- Running Order Test Cases ----> \n";
        echo "\n  <---- Create Order Test Case for Invalid Input ----> \n";
        
        $invalidInput = [
            'origin1' => ['75.745567', '26.905727'],
            'destination' => ['75.747912', '26.906900'],
        ];

        $response = $this->json('POST', '/orders', $invalidInput);
        echo "\n\t <---- should get Status: 400 ----> \n";
        $response->assertStatus(JsonResponse::HTTP_BAD_REQUEST);
    }

    public function testOrderCreationWithInvalidParameters()
    {
        echo "\n  <---- Running Order Test Cases ----> \n";
        echo "\n  <---- Create Order Negative Test Case For Extra Parameters in Lat Long Arrays----> \n";
        
        $invalidInput = [
            'origin' => ['26.905727', '43.958046', '75.745567'],
            'destination' => ['26.906900', '43.958046', '75.747912'],
        ];

        $response = $this->json('POST', '/orders', $invalidInput);
        echo "\n\t <---- should get Status: 400 ----> \n";
        $response->assertStatus(JsonResponse::HTTP_BAD_REQUEST);
    }

    public function testOrderCreationEmptyParameters()
    {
        echo "\n  <---- Running Order Test Cases ----> \n";
        echo "\n  <---- Create Order Negative Test Case For Empty Parameters ----> \n";
       
        $invalidInput = [
            'origin' => ['28.905634', ''],
            'destination' => ['28.905646', '75.748731'],
        ];

        $response = $this->json('POST', '/orders', $invalidInput);
        echo "\n\t <---- should get Status: 400 ----> \n";
        $response->assertStatus(JsonResponse::HTTP_BAD_REQUEST);
    }

    // Order Patch cases
    public function testOrderPatchCases()
    {
        echo "\n \n <---- Running Patch Order Status Positive Test Case ----> \n";
        echo "\n \t > <---- Add new Dummy Order to test Patch Order Status ----> \n";
        
        $validData = [
            'origin' => ['26.905727', '75.745567'],
            'destination' => ['26.906900', '75.747912'],
        ];
        $response = $this->json('POST', '/orders', $validData);
        $data = (array) $response->getData();
        $orderId = $data['id'];

        echo "\n\t <---- Order created: 200 ----> \n";
        $dataToUpdate = ['status' => 'TAKEN'];

        echo "\n \t > <---- Patching above created Order ---->  \n";
        
        $response = $this->json('PATCH', "/orders/{$orderId}", $dataToUpdate);
        $data = (array) $response->getData();
           
        echo "\n > <---- Patch order status should have status 200 ----> \n";
        $response->assertStatus(JsonResponse::HTTP_OK);

        echo "\n > <---- Patch Order status response has key `status` \n";
        $this->assertArrayHasKey('status', $data);

        echo "\n \n <---- Running Patch Order Status Negative Test Case ----> \n";
        echo "\n > <---- Test case for already updated status {$orderId}----> \n";
       
        $response = $this->json('PATCH', "/orders/{$orderId}", $dataToUpdate);
        $data = (array) $response->getData();
        
        echo "\n \t > <---- Trying to patch same order should get status 409 ----> \n";
        $response->assertStatus(JsonResponse::HTTP_CONFLICT);

        echo "\n \t > <---- Trying to patch same order response should has key `error` \n";
        $this->assertArrayHasKey('error', $data);

        echo "\n > <---- Running Patch Order Status Negative Test Cases ----> \n";
        
        echo "\n > <---- (B) - Empty status value ----> \n";
        $this->patchOrderInvalidParams($orderId, ['status' => ''], $expectedCode = JsonResponse::HTTP_BAD_REQUEST);

        echo "\n > <---- (C) - Alpha numeric order id ----> \n";
        $this->patchOrderInvalidParams('Qw12', ['status' => 'TAKEN'], $expectedCode = JsonResponse::HTTP_BAD_REQUEST);

        echo "\n > <---- (D) - Not existing order id ----> \n";
        $this->patchOrderInvalidParams('123432', ['status' => 'TAKEN'], $expectedCode = JsonResponse::HTTP_NOT_FOUND);
    }

    protected function patchOrderInvalidParams($orderId, $params, $expectedCode)
    {
        $response = $this->json('PATCH', '/orders/'.$orderId, $params);
        $data = (array) $response->getData();

        echo "\n \t > <---- Trying to patch Invalid Order response should has status $expectedCode ----> \n";
        $response->assertStatus($expectedCode);

        echo "\n \t > <---- Trying to patch Invalid Order response should has key `error` ----> \n";
        $this->assertArrayHasKey('error', $data);
    }

    public function testGetAllOrdersPositiveCases()
    {
        echo "\n > <---- Get All Orders Positive Test Valid QueryString (page=1&limit=2) ----> \n";
        
        $page = 1;
        $limit = 5;

        $params = ['page' => $page, 'limit' => $limit];
        
        $response = $this->json('GET', "/orders", $params);
        $data = (array) $response->getData();
       
        echo "\n\t > <---- Status should be 200 ---->\n";
        $response->assertStatus(JsonResponse::HTTP_OK);

        echo "\n\t > <---- Response should have order id, distance and status against the fetched orders ----> \n";
        foreach ($data as $order) {
            $order = (array) $order;
            $this->assertArrayHasKey('id', $order);
            $this->assertArrayHasKey('distance', $order);
            $this->assertArrayHasKey('status', $order);
        }
    }

    public function testGetAllOrdersNegativeCases()
    {
        echo "\n > <---- Running get all orders Negative test cases ----> \n";
        
        echo "\n > <---- Get All Orders Negative Test with Invalid QueryString (pagee) ----> \n";
        $queryString = ['pagee' => 1, 'limit' => 5];
        $this->orderNegativeCase($queryString, JsonResponse::HTTP_BAD_REQUEST);

        echo "\n > <---- Get All Orders Negative Test with Invalid QueryString (limitt) ----> \n";
        $queryString = ['pagee' => 1, 'limitt' => 5];
        $this->orderNegativeCase($queryString, JsonResponse::HTTP_BAD_REQUEST);

        echo "\n > <---- Get All Orders Negative Test with Invalid QueryString value (page = 0) ----> \n";
        $queryString = ['pagee' => 0, 'limit' => 5];
        $this->orderNegativeCase($queryString, JsonResponse::HTTP_BAD_REQUEST);

        echo "\n > <---- Get All Orders Negative Test with Invalid QueryString value (limit = 0) ----> \n";
        $queryString = ['pagee' => 1, 'limit' => 0];
        $this->orderNegativeCase($queryString, JsonResponse::HTTP_BAD_REQUEST);

        echo "\n > <---- Get All Orders Negative Test with Invalid QueryString value (page = -2) ----> \n";
        $queryString = ['pagee' => -1, 'limit' => 5];
        $this->orderNegativeCase($queryString, JsonResponse::HTTP_BAD_REQUEST);
    }

    protected function orderNegativeCase($query, $expectedCode)
    {
        $response = $this->json('GET', "/orders", $query);
        $data = (array) $response->getData();

        echo "\n \t > <---- Get all Orders Negative test case response should has status $expectedCode ----> \n";
        $response->assertStatus($expectedCode);

        echo "\n \t > <---- Get all Orders Negative test case response should has key `error` ----> \n";
        $this->assertArrayHasKey('error', $data);
    }
}
