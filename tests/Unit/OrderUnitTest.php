<?php

namespace Tests\Unit;

use App\Http\Controllers\OrderController;
use App\Order;
use Faker\Factory as Faker;
use Illuminate\Http\JsonResponse;
use Tests\TestCase;

class OrderUnitTest extends TestCase
{
    protected static $allowedOrderStatus = [
        Order::UNASSIGNED_ORDER_STATUS,
        Order::ASSIGNED_ORDER_STATUS,
    ];

    public function setUp(): void
    {
        parent::setUp();

        $this->faker = Faker::create();
        $this->orderServiceMock = \Mockery::mock(\App\Http\Services\OrderServices::class);
        $this->responseMock = $this->createResponseMock();

        $this->app->instance(
            OrderController::class,
            new OrderController(
                $this->orderServiceMock,
                $this->responseMock
            )
        );
    }

    public function tearDown(): void
    {
        \Mockery::close();
    }

    public function testCreateOrderForValidData()
    {
        echo "\n <---- Running Unit Test Cases for create order ----> \n\n\n";
        echo "\n <---- Create valid Order ----> \n";
        $order = $this->generateDummyOrder();
        $params = [
            'origin' => [$this->faker->latitude(), $this->faker->longitude()],
            'destination' => [$this->faker->latitude(), $this->faker->longitude()],
        ];

        //Order Service add order will return success
        $this->orderServiceMock
            ->shouldReceive('addOrder')
            ->once()
            ->andReturn($order);

        $response = $this->call('POST', '/orders', $params);
        $data = (array) $response->getData();

        $response->assertStatus(JsonResponse::HTTP_OK);
        $this->assertInternalType('array', $data);
        $this->assertArrayHasKey('id', $data);
        $this->assertArrayHasKey('distance', $data);
    }

    public function testCreateOrderForInvalidData()
    {
        echo "\n <---- Running Unit Test Cases for create order ----> \n\n\n";
        echo "\n <---- Try to Create Invalid Order ----> \n";
        $params = [
            'origin' => [$this->faker->latitude(), $this->faker->longitude()],
            'destination' => [$this->faker->latitude(), $this->faker->longitude()],
        ];

        //Order Service create record will return failure
        $this->orderServiceMock
            ->shouldReceive('addOrder')
            ->once()
            ->andReturn(false);

        $this->orderServiceMock->error = 'INVALID_PARAMETERS';
        $this->orderServiceMock->errorCode = JsonResponse::HTTP_BAD_REQUEST;

        $response = $this->call('POST', '/orders', $params);
        $data = (array) $response->getData();

        $response->assertStatus(JsonResponse::HTTP_BAD_REQUEST);
        $this->assertInternalType('array', $data);
        $this->assertArrayHasKey('error', $data);
    }

    public function testCreateOrderNegativeCaseException()
    {
        echo "\n <---- Create Order with Invalid data (Negative Test Case) for Exception Handling ----> \n";
        $order = $this->generateDummyOrder();

        $params = [
            'origin' => [strval($this->faker->latitude()), strval($this->faker->longitude())],
            'destination' => [strval($this->faker->latitude()), strval($this->faker->longitude())],
        ];

        //Order Service create order will return failure
        $this->orderServiceMock
            ->shouldReceive('addOrder')
            ->once()
            ->andThrow(
                new \InvalidArgumentException()
            );

        $this->orderServiceMock->error = 'Invalid_Argument_Exception';
        $this->orderServiceMock->errorCode = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;

        $response = $this->call('POST', '/orders', $params);
        $data = (array) $response->getData();
        $response->assertStatus(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        $this->assertInternalType('array', $data);
        $this->assertArrayHasKey('error', $data);
    }

    public function testPatchOrderStatusNegativeCaseException()
    {
        echo "\n <---- Patch Order Status for Negative Test Case for Exception Handling ----> \n";
        $id = $this->faker->randomDigit();
        $order = $this->generateDummyOrder($id);

        $this->orderServiceMock
            ->shouldReceive('getOrder')
            ->andThrow(
                new \InvalidArgumentException()
            );

        $params = ['status' => 'TAKEN'];
        $this->orderServiceMock->error = 'Invalid_Argument_Exception';
        $this->orderServiceMock->errorCode = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;

        $response = $this->call('PATCH', "/orders/{$id}", $params);
        $data = (array) $response->getData();

        $response->assertStatus(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        $this->assertInternalType('array', $data);
        $this->assertArrayHasKey('error', $data);
    }

    public function testGetAllOrderNegativeCaseException()
    {
        echo "\n <---- Get All orders for Negative Test Case with Exception Handling ----> \n";
        $page = 1;
        $limit = 7;
        $this->orderServiceMock
            ->shouldReceive('fetchAllOrders')
            ->with($page, $limit)
            ->andThrow(
                new \InvalidArgumentException()
            );

        $params = ['page' => $page, 'limit' => $limit];
        $this->orderServiceMock->error = 'Invalid_Argument_Exception';
        $this->orderServiceMock->errorCode = JsonResponse::HTTP_INTERNAL_SERVER_ERROR;

        $response = $this->call('GET', '/orders', $params);
        $data = (array) $response->getData();

        $response->assertStatus(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        $this->assertInternalType('array', $data);
        $this->assertArrayHasKey('error', $data);
    }

    public function testGetAllOrderWithValidParams()
    {
        echo "\n\n\n <---- Get All Orders ---->  \n";
        $page = 1;
        $limit = 4;
        $orderList = [];

        for ($i = 0; $i < 4; ++$i) {
            $orderList[] = $this->generateDummyOrder();
        }
        $orderRecordCollection = new \Illuminate\Database\Eloquent\Collection($orderList);
        $this->orderServiceMock
            ->shouldReceive('fetchAllOrders')
            ->once()
            ->with($page, $limit)
            ->andReturn($orderRecordCollection);

        
        $params = ['page' => $page, 'limit' => $limit];
        $response = $this->call('GET', "/orders", $params);
        $data = (array) $response->getData();

        echo "\n\t > <---- Status should be 200 ---->\n";
        $response->assertStatus(JsonResponse::HTTP_OK);
        echo "\n\t > <---- Response should have order id, distance and status against the fetched orders ----> \n";
        $this->assertInternalType('array', $data);
        foreach ($data as $order) {
            $order = (array) $order;
            $this->assertArrayHasKey('id', $order);
            $this->assertArrayHasKey('distance', $order);
            $this->assertArrayHasKey('status', $order);
        }
    }

    private function generateDummyOrder($id = null)
    {
        $id = $id ?: $this->faker->randomDigit();

        $order = new Order();
        $order->id = $id;
        $order->origin_start_latitude = $this->faker->latitude();
        $order->origin_end_longitude = $this->faker->longitude();
        $order->destination_start_latitude = $this->faker->latitude();
        $order->destination_end_longitude = $this->faker->longitude();
        $order->distance = $this->faker->numberBetween(1000, 9999);
        $order->status = $this->faker->randomElement(self::$allowedOrderStatus);
        $order->created_at = $this->faker->dateTimeBetween();
        $order->updated_at = $this->faker->dateTimeBetween();

        return $order;
    }

    /**
     * @return Mockery_2_App_Http_Response_Response
     */
    private function createResponseMock()
    {
        $messageHelperMock = \Mockery::mock('\App\Helpers\MessageHelper')->makePartial();

        $responseMock = \Mockery::mock('\App\Http\Response\ResponseHelper[formatOrderResponse]', [$messageHelperMock]);

        $responseMock
            ->shouldReceive('formatOrderResponse')
            ->andReturnUsing(function ($argument) {
                return [
                    'id' => $argument->id,
                    'status' => $argument->status,
                    'distance' => $argument->distance,
                ];
            });

        return $responseMock;
    }
}
